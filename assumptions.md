
# Assumptions

1. Processing needs to happen parallely for number of beverages
2. The input.json has the beverages as a JSON, but the language's default read order of hash is the order in which they
   need to be processed
3. We have to write integration tests only and not unit tests
4. add_stock method is to be called manually according to the testcase
5. Threads are spawned according to the number of outlets in one batch and the machine on which the code runs can handle the number of threads
6. If any one of the ingredient is not present, we return from there instead of finding all the ingredients which are not available
