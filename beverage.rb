class Beverage
  attr_reader :name, :ingredients
  ##
  # Class to represent a beverage

  def initialize(name, ingredients)
    # Constructor for the Beverage
    # name --> Name of the beverage
    # ingredients --> Ingredients required to prepare the beverage

    @name = name
    @ingredients = ingredients
  end

  def serve_beverage(curr_stock)
    # Given the current stock of the vending machine
    # return the status of the processing

    # Shuffle because we can pick ingredients available randomly
    beverage_ingredients = @ingredients.keys.shuffle
    beverage_ingredients.each do |ingredient|
      quantity = @ingredients[ingredient]

      return response_value("NOT_FOUND", ingredient) if !curr_stock.include?(ingredient)

      if quantity <= curr_stock[ingredient]
        # Keep subtracting the quantity when it is processed
        curr_stock[ingredient] -= quantity
      else
        return response_value("NOT_SUFFICIENT", ingredient)
      end
    end

    return response_value("SUCCESS", nil, curr_stock)
  end

  def response_value(status, ingredient, curr_stock=nil)
    # Respond with a hash to the parent for printing in the required format
    {
      name: @name,
      status: status,
      ingredient: ingredient,
      stock_left: curr_stock
    }
  end
end