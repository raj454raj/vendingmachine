# Vending Machine

## Navigation

1. `input.json` - File which contains the input test case
2. `vending_machine.rb` - Starting point of the project
3. `spec/` - Directory containing functional test cases
4. `assumptions.md` - Assumptions considered while designing the code

## Usage

* Individual run
```
$ bundle exec ruby vending_machine.rb
```

* Infinite loop run
```
$ while true; do bundle exec ruby vending_machine.rb; echo "_________________"; sleep 1; done
```

* Run the functional test cases
```
$ bundle exec rspec
```

## Setup

1. Install rvm
2. `.ruby-version` and `.ruby-gemset` files are already committed, so it will already create a gemset if the ruby version is installed on your system
3. Install the gems required for the project
```
bundle install
```
