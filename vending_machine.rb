require 'json'
require 'pry-nav'
require_relative('./beverage.rb')

class VendingMachine
  ##
  # Processing beverage orders on a given number of outlets
  # with a limited stock on ingredients available

  attr_reader :stock_items

  def initialize(outlet_count, beverages, stock_items)
    # Constructor for the vending machine
    # outlet_count --> Number of outlets in the machine
    # beverages --> List of beverages to process
    # stock_items --> Total amount of ingredients available

    @number_of_outlets = outlet_count
    @mutex = Mutex.new
    @beverages = []
    @stock_items = stock_items
    @stock_items.default = 0
    beverages.each { |beverage, ingredients|
      # Order of the beverages will now be freezed based on the input hash
      @beverages << Beverage.new(beverage, ingredients)
    }
  end

  def add_stock(ingredient, amount)
    # Add stock of a particular ingredient

    @mutex.synchronize {
      @stock_items[ingredient] += amount
    }
  end

  def process_batch(batch)
    # Process one batch of beverages in parallel based on the number of outlets

    threads = []
    batch.each do |beverage|
      threads << Thread.new {
        @mutex.synchronize {
          # Run the processing of a beverage in a mutex as we don't want stock
          # to be read and written in parallel
          process_beverage(beverage)
        }
      }
    end
    threads.map(&:join)
  end

  def process_orders
    # Process given beverages in parallel based on the number of outlets

    i = 0
    while i < @beverages.length
      puts "Processing batch #{i / @number_of_outlets + 1} ..."

      # Take the batch of size of number of outlets
      batch = @beverages.slice(i, i + @number_of_outlets)
      process_batch(batch)

      i += @number_of_outlets
    end
  end

  def process_beverage(beverage)
    # Process one beverage according to the given stock

    curr_stock = @stock_items.clone
    result = beverage.serve_beverage(curr_stock)
    print_message(beverage.name, result)

    # Take the stock left from the serve_beverage call if the beverage's ingredients
    # were available and beverage was successfully prepared
    @stock_items = result[:stock_left] if result[:status] == "SUCCESS"
  end

  def print_message(beverage_name, result)
    # Format the message required for the output given the beverage and status

    if result[:status] == "NOT_SUFFICIENT"
      puts "#{beverage_name} cannot be prepared because #{result[:ingredient]} is not sufficient"
    elsif result[:status] == "NOT_FOUND"
      puts "#{beverage_name} cannot be prepared because #{result[:ingredient]} is not available"
    else
      puts "#{beverage_name} is prepared"
    end
  end
end

if __FILE__ == $0
  input = JSON.load(File.new("input.json"))

  vending_machine = VendingMachine.new(
                      input["machine"]["outlets"]["count_n"],
                      input["machine"]["beverages"],
                      input["machine"]["total_items_quantity"]
                    )

  vending_machine.process_orders
end
