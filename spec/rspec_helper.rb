require 'pry-nav'

module Helper
  def capture_stdout(&block)
    begin
      $stdout = StringIO.new
      $stderr = StringIO.new
      yield
      result = {}
      result[:stdout] = $stdout.string
      result[:stderr] = $stderr.string
    ensure
      $stdout = STDOUT
      $stderr = STDERR
    end
    result
  end
end

RSpec.configure do |config|
  config.include Helper
end