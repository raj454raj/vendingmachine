require_relative "./rspec_helper.rb"
require_relative "../vending_machine.rb"

describe VendingMachine do
  context "enough quantity for the beverages is present" do
    beverages = {
      "hot_tea": {
        "hot_water": 200,
        "hot_milk": 100,
        "tea_leaves_syrup": 30
      },
      "hot_coffee": {
        "hot_water": 100,
        "hot_milk": 400,
        "tea_leaves_syrup": 30
      },
      "black_tea": {
        "hot_water": 300,
        "tea_leaves_syrup": 30
      }
    }

    stock_items = {
      "hot_water": 600,
      "hot_milk": 500,
      "tea_leaves_syrup": 100
    }

    it "should return 'is prepared' for all the beverages in any number of executions" do
      10.times do
        vending_machine = VendingMachine.new(3, beverages.clone, stock_items.clone)
        standard_output = capture_stdout { vending_machine.process_orders }[:stdout]

        beverages.each do |beverage, _|
          expect(standard_output).to match(/#{beverage} is prepared/)
        end
      end
    end
  end

  context "enough quantity for the beverage is not present" do
    beverages = {
      "hot_tea": {
        "hot_water": 200,
        "hot_milk": 100,
        "tea_leaves_syrup": 30
      },
      "hot_coffee": {
        "hot_water": 100,
        "hot_milk": 400,
        "tea_leaves_syrup": 30
      },
      "black_tea": {
        "hot_water": 500,
        "tea_leaves_syrup": 30
      }
    }

    stock_items = {
      "hot_water": 600,
      "hot_milk": 500,
      "tea_leaves_syrup": 100
    }

    it "should return 'is not sufficient' if not enough quantity for one of the beverage" do
      10.times do
        vending_machine = VendingMachine.new(3, beverages.clone, stock_items.clone)
        standard_output = capture_stdout { vending_machine.process_orders }[:stdout]
        expect(standard_output.gsub("is prepared").count).to eq(2)
        expect(standard_output.gsub("is not sufficient").count).to eq(1)
      end
    end
  end

  context "ingredient for a beverage not present" do
    beverages = {
      "hot_tea": {
        "hot_water": 200,
        "hot_milk": 100,
        "tea_leaves_syrup": 30
      },
      "hot_coffee": {
        "hot_water": 100,
        "hot_milk": 400,
        "tea_leaves_syrup": 30
      },
      "black_tea": {
        "hot_water": 100,
        "tea_leaves_syrup": 30,
        "ginger_syrup": 10
      }
    }

    stock_items = {
      "hot_water": 600,
      "hot_milk": 500,
      "tea_leaves_syrup": 100
    }

    it "should give 'is not available' if ingredient is not present in stock" do
      10.times do
        vending_machine = VendingMachine.new(3, beverages.clone, stock_items.clone)
        standard_output = capture_stdout { vending_machine.process_orders }[:stdout]
        expect(standard_output.gsub("is prepared").count).to eq(2)
        expect(standard_output.gsub("is not available").count).to eq(1)
      end
    end
  end

  context "multiple batch processing" do
    beverages = {
      "hot_tea": {
        "hot_water": 200,
        "hot_milk": 100,
        "tea_leaves_syrup": 30
      },
      "hot_coffee": {
        "hot_water": 100,
        "hot_milk": 200,
        "tea_leaves_syrup": 30
      },
      "black_tea": {
        "hot_water": 100,
        "tea_leaves_syrup": 30,
        "ginger_syrup": 10
      },
      "green_tea": {
        "hot_water": 100,
        "ginger_syrup": 30,
        "sugar_syrup": 50
      }
    }

    stock_items = {
      "hot_water": 600,
      "hot_milk": 500,
      "tea_leaves_syrup": 100,
      "ginger_syrup": 20,
      "sugar_syrup": 50
    }

    it "should return 'not sufficient' over multiple batches" do
      vending_machine = VendingMachine.new(3, beverages.clone, stock_items.clone)
      standard_output = capture_stdout { vending_machine.process_orders }[:stdout]
      expect(standard_output).to match(/Processing batch 1/)
      expect(standard_output).to match(/Processing batch 2/)
      expect(standard_output).not_to match(/Processing batch 3/)
      expect(standard_output).to match(/hot_tea is prepared/)
      expect(standard_output).to match(/hot_coffee is prepared/)
      expect(standard_output).to match(/black_tea is prepared/)
      expect(standard_output).to match(/green_tea.*is not sufficient/)
    end
  end

  context "add_stock" do
    beverages = {
      "hot_tea": {
        "hot_water": 200,
        "hot_milk": 100,
        "tea_leaves_syrup": 30
      },
      "hot_coffee": {
        "hot_water": 100,
        "hot_milk": 200,
        "tea_leaves_syrup": 30
      },
      "black_tea": {
        "hot_water": 100,
        "tea_leaves_syrup": 30,
        "ginger_syrup": 10
      },
      "green_tea": {
        "hot_water": 100,
        "ginger_syrup": 30,
        "sugar_syrup": 50
      }
    }

    stock_items = {
      "hot_water": 600,
      "hot_milk": 500,
      "tea_leaves_syrup": 100,
      "ginger_syrup": 20,
      "sugar_syrup": 50
    }

    it "should update the stock with add_stock" do
      vending_machine = VendingMachine.new(3, beverages.clone, stock_items.clone)
      expect(vending_machine.stock_items[:hot_water]).to eq(600)
      vending_machine.add_stock(:hot_water, 100)
      expect(vending_machine.stock_items[:hot_water]).to eq(700)
    end
  end
end